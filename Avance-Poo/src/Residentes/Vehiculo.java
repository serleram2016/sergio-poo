/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Residentes;

/**
 *
 * @author CltControl
 */
public class Vehiculo {
    String nPlaca, nMatricula, nomPropietario;

    public Vehiculo(String nPlaca, String nMatricula, String nomPropietario) {
        this.nPlaca = nPlaca;
        this.nMatricula = nMatricula;
        this.nomPropietario = nomPropietario;
    }

    public String getnPlaca() {
        return nPlaca;
    }

    public String getnMatricula() {
        return nMatricula;
    }

    public String getNomPropietario() {
        return nomPropietario;
    }
    
    
    
}
