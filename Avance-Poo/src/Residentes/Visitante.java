/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Residentes;

import java.util.Date;

/**
 *
 * @author CltControl
 */
public class Visitante {
    String nombre, cedula, correo;
    Date fechaInicio;

    public Visitante(String nombre, String cedula, String correo, Date fechaInicio) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.correo = correo;
        this.fechaInicio = fechaInicio;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }
    
    
}
