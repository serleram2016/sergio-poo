/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Residentes;

import java.util.ArrayList;

/**
 *
 * @author CltControl
 */
public class Residente {
    String nombre, correo, telefono, pinAcceso;
    ArrayList<Visitante> visitantes;
    ArrayList<Vehiculo> vehiculos;
    

    public Residente(String nombre, String correo, String telefono, String pinAcceso) {
        this.nombre = nombre;
        this.correo = correo;
        this.telefono = telefono;
        this.pinAcceso = pinAcceso;
    }
    
    public boolean registrasVisitante(Visitante v){
        for(int i=0; i<visitantes.size();i++){
          if(!v.getCedula().equals(visitantes.get(i).getCedula())){
            visitantes.add(v);
            return true;
        }    
        }
        return false;  
                
    }
    

    public String getNombre() {
        return nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getPinAcceso() {
        return pinAcceso;
    }
    
    
    
    
    
}
